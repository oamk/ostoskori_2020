<?php
session_start();
session_regenerate_id();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verkkokauppa</title>
</head>
<body>
  <h3>Tilaus</h3>
  <?php
  try {
    $tietokanta = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8', 'root', 'root1234');
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $tietokanta->beginTransaction();
    $etunimi = filter_input(INPUT_POST,'etunimi',FILTER_SANITIZE_STRING);
    $sukunimi = filter_input(INPUT_POST,'sukunimi',FILTER_SANITIZE_STRING);

    // 1. Tallennetaan asiakas.
    $sql = "insert into asiakas (etunimi, sukunimi) values (:etunimi,:sukunimi)";
    $kysely = $tietokanta->prepare($sql);
    $kysely->bindValue(':etunimi',$etunimi,PDO::PARAM_STR);
    $kysely->bindValue(':sukunimi',$sukunimi,PDO::PARAM_STR);
    $kysely->execute();
    $asiakas_id = $tietokanta->lastInsertId();

    // 2. Tallennetaan tilaus.
    $sql = "insert into tilaus (asiakas_id) values (:asiakas_id)";
    $kysely = $tietokanta->prepare($sql);
    $kysely->bindValue(':asiakas_id',$asiakas_id,PDO::PARAM_INT);
    $kysely->execute();
    $tilaus_id = $tietokanta->lastInsertId();
    //echo "Tilaus id on $tilaus_id<br>";
    // 3. Tallennetaan tilausrivit.
    $tuotteet = $_SESSION['kori'];
    foreach ($tuotteet as $tuote_id) {
      $sql = "insert into tilausrivi (tilaus_id,tuote_id) values (:tilaus_id,:tuote_id)";
      $kysely = $tietokanta->prepare($sql);
      $kysely->bindValue(':tilaus_id',$tilaus_id,PDO::PARAM_INT);
      $kysely->bindValue(':tuote_id',$tuote_id,PDO::PARAM_INT);
      $kysely->execute();
    }
    $_SESSION['kori'] = null;
    $tietokanta->commit();
    print "<p>Kiitos tilauksesta</p>";

  } catch (PDOException $pdoex) {
    $tietokanta->rollback();
    print "<p>Häiriö verkkokaupassa!</p>";
    print "<p>" . $pdoex->getMessage() . "</p>";
  }
?>
</body>
</html>