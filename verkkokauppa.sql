drop database if exists verkkokauppa;
create database verkkokauppa;
use verkkokauppa;

create table tuoteryhma (
  id int primary key auto_increment,
  nimi varchar(50) not null unique
);

create table tuote (
  id int primary key auto_increment,
  nimi varchar(100) not null,
  hinta decimal(5,2) not null,
  kuvaus text,
  varastomaara int not null,
  tuoteryhma_id int not null,
  index (tuoteryhma_id),
  foreign key (tuoteryhma_id) references tuoteryhma(id)
  on delete restrict
);

create table asiakas (
  id int primary key auto_increment,
  etunimi varchar(100) not null,
  sukunimi varchar(100) not null
);

create table tilaus (
  id int primary key auto_increment,
  tilattu timestamp default current_timestamp,
  asiakas_id int not null,
  index (asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);

create table tilausrivi (
  tilaus_id int not null,
  index (tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index (tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict
);


insert into tuoteryhma (nimi) values ('Asusteet');
insert into tuoteryhma (nimi) values ('Urheiluvälineet');

insert into tuote (nimi,hinta,varastomaara,tuoteryhma_id) values 
('Takki',50,6,1);

insert into tuote (nimi,hinta,varastomaara,tuoteryhma_id) values 
('Housut',75,3,1);

insert into tuote (nimi,hinta,varastomaara,tuoteryhma_id) values 
('Lakki',15,9,1);

insert into tuote (nimi,hinta,varastomaara,tuoteryhma_id) values 
('Luistimet',775,3,2);

insert into tuote (nimi,hinta,varastomaara,tuoteryhma_id) values 
('Jalkapallo',95,8,2);
