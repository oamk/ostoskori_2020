<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verkkokauppa</title>
</head>
<body>
  <h3>Tilaus</h3>
  <form action="tilaa.php" method="post">
    <div>
      <label>Etunimi</label>
      <input name="etunimi">
    </div>
    <div>
      <label>Sukunimi</label>
      <input name="sukunimi">
    </div>
    <button>Tilaa</button>
  </form>
</body>
</html>