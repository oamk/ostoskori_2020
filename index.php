<?php
session_start();
session_regenerate_id();
$tuoteryhma_id = 0;

// Tarkastetaan, onko tyhjennä-linkkiä painettu.
// Jos on, niin tuhotaan kori-istuntomuuttuja.
if (isset($_GET['tyhjenna'])) {
  $_SESSION['kori'] = null;
}

// Tarkastetaan, onko osoiterivillä välitetty valitun tuoteryhmän id, jotta osataan hakea tuotteet
// ryhmän mukaan.
if (isset($_GET['tuoteryhma_id'])) {
  $tuoteryhma_id = filter_input(INPUT_GET, 'tuoteryhma_id', FILTER_SANITIZE_NUMBER_INT);
}

// Jos ostoskoria ei ole (1. kutsukerta tai tyhjennyksen jälkeen), luodaan uusi tyhjä ostoskori.
if (!isset($_SESSION['kori'])) {
  $_SESSION['kori'] = array();
}

// Jos osta-painiketta painettu, lisätään tuote (id) ostoskoriin. Piilokenttä voi sisältää myös
// valitun tuoteryhmän, mikäli käyttäjä oli selaamassa jonkin tuoteryhmän tuotteita. Valinta ei saa
// "hävitä", joten tieto on tallennettu piilokenttään lomakkeelle, josta se on luettavissa.
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $tuotteet = $_SESSION['kori'];
  $tuote = filter_input(INPUT_POST, 'tuote', FILTER_SANITIZE_NUMBER_INT);
  $tuoteryhma_id = filter_input(INPUT_POST, 'tuoteryhma', FILTER_SANITIZE_NUMBER_INT);
  array_push($tuotteet, $tuote);
  $_SESSION['kori'] = $tuotteet;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Verkkokauppa</title>
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <?php
  // Avataan tietokanta ja näytetään mahdollinen virhe.
  try {
    $tietokanta = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8', 'root', 'root1234');
    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $pdoex) {
    print "<p>Häiriö verkkokaupassa!</p>";
  }
  ?>
  <!-- Tuoteryhmät sivun vasemmassa reunassa. -->
  <div id="tuoteryhma" style="display: inline;float: left; width: 20%;">
    <?php
    // Tulostetaan tuoteryhmät.
    try {
      $sql = "select * from tuoteryhma";
      $kysely = $tietokanta->query($sql);
      print "<ul>";
      while ($tietue = $kysely->fetch()) {
        print "<li><a href='" . $_SERVER['PHP_SELF'] . "?tuoteryhma_id=" . $tietue['id'] . "'>" . $tietue['nimi'] . "</a></li>";
      }
      print "</ul>";
    } catch (PDOException $pdoex) {
      print "<p>Häiriö verkkokaupassa!</p>";
    }

    ?>
  </div>
  <!-- Tuotteet sivun keskellä. -->
  <div id="tuotteet" style="display: inline;float: left; width: 60%">
    <h3>Verkkokauppa</h3>
    <?php
    // Haetaan tuotteet. Jos tuoteryhmä on asetettu, rajataan haettavia tuotteita ryhmällä.
    try {
      $sql = "select * from tuote";
      if ($tuoteryhma_id > 0) {
        $sql .= " where tuoteryhma_id = $tuoteryhma_id";
      }
      $kysely = $tietokanta->query($sql);
      while ($tietue = $kysely->fetch()) {
        print "<div>";
        print "<form method='post' action='" . $_SERVER['PHP_SELF']  . "'>";
        // Lisätään tuotteen id piilokenttään, jotta se on luettavissa, kun osta-painiketta painetaan.
        print "<input type='hidden' name='tuote' value='" . $tietue['id'] . "'>";
        // Jos tuoteryhmä oli asetettu, lisätään piilokenttä, jotta ostoskoriin lisäämisenkin jälkeen tuoteryhmän valinta on "voimassa".
        if ($tuoteryhma_id > 0) {
          print "<input type='hidden' name='tuoteryhma' value='" . $tuoteryhma_id . "'>";
        }
        print "<p>" . $tietue['nimi'] . "</p>";
        print "<p>" . $tietue['hinta'] . "</p>";
        print "<button>Osta</button>";
        print "</form>";
        print "</div>";
      }
    } catch (PDOException $pdoex) {
      print "<p>Häiriö verkkokaupassa!</p>";
    }
    ?>
    </div>
    <!-- Ostoskori sivun oikeassa reunassa. -->
    <div style="display: inline;width: 20%">
    <?php
    // Käydään ostoskori läpi (tuotteiden idt) ja tulostetaan ostoskorin sisältö.
    print "<p>Ostoskorissa on "  .  count($_SESSION['kori']) . " tuotetta</p>";
    $tuotteet = $_SESSION['kori'];
    $summa = 0;
    foreach ($tuotteet as $tuote_id) {
      $sql = "select * from tuote where id = $tuote_id";
      $kysely = $tietokanta->query($sql);
      $tuote = $kysely->fetch();
      $summa +=  $tuote['hinta'];
      print $tuote['nimi'] . ' ' . $tuote['hinta'] .  '<br />';
    }
    print "<p>Yhteensä: $summa</p>";
    ?>
    <a href="<?php print($_SERVER['PHP_SELF']); ?>?tyhjenna=true">Tyhjennä</a>
    <a href="tilaus.php">Tilaa</a>
    </div>
</body>

</html>